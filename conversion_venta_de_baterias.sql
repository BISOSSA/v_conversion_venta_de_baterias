CREATE OR REPLACE VIEW conversion_venta_de_baterias AS
/*PASOS PARA MODIFICACIONES EN CALCULOS:
1- AGREGAR O AGRUPAR LAS LOCALIDADES, PROVINCIAS O ZONAS NUEVAS EN EL CALCULO DE "LOCALIDAD"
2- AGREGAR LAS LOCALIDADES, PROVINCIAS O ZONAS EN EL "WHERE"
3- APLICAR LOGICA DE SERVICIOS EN EL CALCULO "REALIZADOS" SEGUN LAS LOCALIDADES, PROVINCIAS O ZONAS Y DIA QUE COMIENZAN LAS VENTAS SI CORRESPONDE*/



SELECT to_char(s.fech_serv, 'MM/DD/YYYY', 'NLS_DATE_LANGUAGE = SPANISH') fech_serv
      ,SYSDATE actualizacion_conversion
      ,s.nume_serv nrosvc
      ,to_number(to_char(s.fech_serv, 'YYYYMM')) idmes
      ,to_char((s.fech_serv), 'MONTHYYYY', 'NLS_DATE_LANGUAGE = SPANISH') ds_mes
      ,to_char(s.fech_serv, 'DAY', 'NLS_DATE_LANGUAGE = SPANISH') dia_de_semana
      ,to_char(sh.hora_pedi, 'HH24') hora_del_dia
      ,COUNT(DISTINCT s.nume_serv) solicitados
      ,
       
       COUNT(DISTINCT CASE
               WHEN s.nume_tipo_serv IN (1, 2) THEN
                s.nume_serv
             END) solicitados_mec_trasl
      ,COUNT(DISTINCT CASE
               WHEN s.nume_tipo_serv = 1 THEN
                s.nume_serv
             END) solicitados_mec
      ,COUNT(DISTINCT CASE
               WHEN s.nume_diag_esti = 151 THEN
                s.nume_serv
             END) solicitados_pot_arr
      ,
       
       --        COUNT (DISTINCT CASE WHEN  S.NUME_CIER >= 100/*BETWEEN 100 AND 110*/  THEN S.NUME_SERV END) REALIZADOS,
       COUNT(DISTINCT CASE
               WHEN sg.nume_diag = 151 THEN
                (CASE
                  WHEN s.nume_cier >= 100 AND s.nume_serv < 20181010100000 AND (l.nume_regi = 3 OR s.nume_serv = 20181013301170) THEN
                   s.nume_serv --Solo Cordoba
                  WHEN s.nume_cier >= 100 AND s.nume_serv >= 20181010100000 AND s.nume_serv <= 20181231999999 AND
                       (l.nume_loca NOT IN (3084
                                           , --MAR DEL PLATA
                                            3106
                                           , --CHAPADMALAL
                                            2397
                                           , --MIRAMAR (BOLIVAR-PDO. BOLIVAR)
                                            3116
                                           , --MIRAMAR (PDO. GRAL. ALVARADO)
                                            3125
                                           , --SANTA CLARA DEL MAR
                                            3180
                                           , --BAHIA BLANCA
                                            3130
                                           , --CAMET
                                            3086
                                           , --BATAN
                                            2777
                                           , --GENERAL PIRAN
                                            3093
                                           , --SIERRA DE LOS PADRES
                                            2654
                                           , --SANTA TERESITA
                                            1723) --LA PLATA
                       OR zon.nume_macr_zona NOT IN (3, 4, 5, 6, 7)) --CF, GBA Y LA PLATA 
                   THEN
                   s.nume_serv --Cordoba y Rosario
                  WHEN s.nume_cier >= 100 AND s.nume_serv >= 20190101100000 AND s.nume_serv <= 20190131999999 AND (l.nume_loca <> 1723 --LA PLATA
                       OR zon.nume_macr_zona NOT IN (3, 4, 5, 6, 7)) --CF, GBA Y LA PLATA 
                   THEN
                   s.nume_serv -- Cordoba, Rosario, Mar del Plata
                -------------NUEVO-------------------
                  WHEN s.nume_cier >= 100 AND s.nume_serv >= 20190201100000 AND s.nume_serv <= 20190231999999 AND (l.nume_prov NOT IN (14) -- NEUQUEN
                       OR l.nume_loca NOT IN (11907) -- CIPOLLETTI
                       OR l.nume_loca NOT IN (11896) -- CATRIEL
                       OR zon.nume_macr_zona NOT IN (3, 5, 6, 7)) --CF Y GBA 
                   THEN
                   s.nume_serv -- Cordoba, Rosario, Mar del Plata, La Plata
                
                ----------------TODOS---------------         
                  WHEN s.nume_cier >= 100 AND s.nume_serv >= 20190301100000 THEN
                   s.nume_serv -- Cordoba, Rosario, Mar del Plata, La Plata, Villa Carlos Paz, Neuquen, CF y GBA
                END)
             END) realizados
      ,COUNT(DISTINCT CASE
               WHEN sg.nume_diag <> 151 THEN
                bat.nume_serv
             END) venta_en_otros_diag
      ,r.nomb_regi region_svc
      ,nvl(loca.localidades_agrupadas, 'A AGRUPAR ' || l.nomb_loca) localidad
      ,
       --        L.NOMB_LOCA LOCALIDAD,
       sd.nume_lati      latitud
      ,sd.nume_long      longitud
      ,co.nomb_comp      compania
      ,co.nume_comp      companianum
      ,ts.nomb_tipo_serv
      ,ss.nomb_titu
      ,ss.tele_titu
      ,ss.pate_vehi_actu
      ,ss.marc_vehi
      ,cr.nomb_radi      radio_compania
      ,
       -----nuevo campo 22/10/18
       COUNT(DISTINCT bat.nume_serv) venta
      ,COUNT(CASE
               WHEN cr.nume_radi = 18 THEN
                bat.nume_serv
             END) vta_sos_energy
      ,COUNT(CASE
               WHEN cr.nume_radi = 18 THEN
                s.nume_serv
             END) svc_sos_energy
      ,
       ------ 
       ss.mode_vehi
      ,ss.tipo_tele_titu
      ,ss.mail_soci
      ,ss.codi_iden_auto
      ,
       --        SG.NOMB_DIAG, 
       CASE
         WHEN bat.nume_serv = s.nume_serv AND s.nume_diag_esti <> 151 THEN
          'ARRANQUE EN GENERAL(POTENCIAL VENTA)'
         ELSE
          sg.nomb_diag
       END nomb_diag
      ,zon.nomb_zona zonasv
      ,pers.nomb_pers derivador
      ,p.nomb_pres nombre_prestador
      ,dba_sosdw.nombre_sede_operacion(s.nume_tele) || ' - ' || dba_sos.nomb_pues_ulti(s.nume_tele) sede_puesto
      ,
       --       DBA_SOS.NOMB_SEDE_OPER (S.NUME_TELE) sede_operativa,
       (SELECT dr.nomb_diag
          FROM diagnosticos dr
         WHERE dr.nume_diag = s.nume_diag_real) diagnostico_real
      ,CASE
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 1 AND 7 THEN
          1
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 8 AND 14 THEN
          2
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 15 AND 21 THEN
          3
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 22 AND 28 THEN
          4
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 29 AND 31 THEN
          5
       END id_semanas_del_mes
      ,CASE
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 1 AND 7 THEN
          'SEMANA 1'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 8 AND 14 THEN
          'SEMANA 2'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 15 AND 21 THEN
          'SEMANA 3'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 22 AND 28 THEN
          'SEMANA 4'
         WHEN to_char(s.fech_serv, 'DD') BETWEEN 29 AND 31 THEN
          'SEMANA 5'
       END semanas_del_mes
  FROM (SELECT *
          FROM stockmovimientos
         WHERE impo_fina_cobr >= 0
           AND nume_serv IS NOT NULL
        /*ORDER BY FECH_OPER_ULTI DESC*/
        ) bat --ON S.NUME_SERV=BAT.NUME_SERV 
 RIGHT JOIN dba_sos.servicios s
    ON s.nume_serv = bat.nume_serv
--LEFT JOIN DBA_SOS.SERVICIOSHORARIOSREALES SHR ON S.NUME_SERV = SHR.NUME_SERV
--LEFT JOIN DBA_SOS.LOG_SERVICIOSHORARIOS SHLOG ON SHLOG.NUME_SERV=S.NUME_SERV  
  LEFT JOIN dba_sos.bases b
    ON b.nume_base = s.nume_base_fact
  LEFT JOIN dba_sos.moviles m
    ON m.nume_movi = s.nume_movi
  LEFT JOIN dba_sos.prestadores p
    ON p.nume_pres = b.nume_pres
  LEFT JOIN dba_sos.tiposcontratos tc
    ON m.nume_tipo_cont = tc.nume_tipo_cont
  LEFT JOIN dba_sos.serviciosdirecciones sd
    ON sd.nume_serv = s.nume_serv
   AND sd.nume_tipo_dire = 1
  LEFT JOIN dba_sos.localidades l
    ON l.nume_loca = sd.nume_loca
  LEFT JOIN dba_sos.zonas zon
    ON zon.nume_zona = l.nume_zona --> PARA IDENTIFICAR LOS DIFERENTES BARRIOS DE LA PLATA
  LEFT JOIN provincias prov
    ON prov.nume_prov = l.nume_prov
  LEFT JOIN dba_sos.regiones r
    ON r.nume_regi = l.nume_regi
  LEFT JOIN dba_sos.tiposservicios ts
    ON ts.nume_tipo_serv = s.nume_tipo_serv
  LEFT JOIN dba_sos.personal pers
    ON pers.nume_pers = s.nume_deri
--                                                                LEFT JOIN DBA_SOS.ZONAS Z ON Z.NUME_ZONA = L.NUME_ZONA
  LEFT JOIN dba_sos.serviciossocios ss
    ON ss.nume_serv = s.nume_serv
  LEFT JOIN dba_sos.companiasradios cr
    ON ss.nume_radi = cr.nume_radi
  LEFT JOIN dba_sos.companias co
    ON cr.nume_comp = co.nume_comp
  LEFT JOIN dba_sos.diagnosticos sg
    ON sg.nume_diag = s.nume_diag_esti --or SG.NUME_DIAG = S.NUME_DIAG_REAL
  LEFT JOIN servicioshorarios sh
    ON sh.nume_serv = s.nume_serv
    JOIN localidades_agrupadas loca ON sd.nume_loca = loca.nume_loca
--                                                                                LEFT JOIN SERVICIOSPRODUCTOS SP ON SP.NUME_SERV = S.NUME_SERV AND SP.NUME_PROD IN (123, 124)
 WHERE s.nume_serv >= 20180401100000 --TO_CHAR(S.FECH_SERV, 'YYYYMM')>=201801  --20190117302679 VTA SOS ENERGY
   AND (s.nume_cier >= 100 OR (SELECT SUM(sp.impo_corr)
                                 FROM serviciosproductos sp
                                 JOIN productos prod
                                   ON prod.nume_prod = sp.nume_prod
                                 JOIN productoscategorias pc
                                   ON pc.nume_cate = prod.nume_cate
                                  AND pc.clas_cate = 'P'
                                WHERE sp.nume_serv = s.nume_serv) > 0)
   AND (l.nume_regi IN (3, 2) --CORDOBA Y ROSARIO
       OR zon.nume_macr_zona = 4 --TODA LA PLATA
       OR zon.nume_macr_zona = 7 --ZONA SUR
       OR zon.nume_macr_zona = 3 -- ZONA CENTRO
       OR zon.nume_macr_zona = 6 --ZONA NORTE
       OR zon.nume_macr_zona = 5 --ZONA OESTE    
       
       OR l.nume_loca IN (1797
                          , --CORONEL BRANDSEN
                           16176
                          , --MENDIOLAZA
                           16407
                          , --SALDAN
                           16185
                          , --UNQUILLO
                           16423
                          , --VILLA CARLOS PAZ
                           16430
                          , --CUESTA BLANCA  
                           16442
                          , --VILLA INDEPENDENCIA
                           16436
                          , --SAN ANTONIO DE ARREDONDO
                           16473
                          , --PARQUE SIQUIMAN
                           16425
                          , --VILLA SANTA CRUZ DEL LAGO
                           12420
                          , -- IBARLUCEA
                           12357
                          , -- SOLDINI
                           12348
                          , -- LA CAROLINA
                           3084
                          , --MAR DEL PLATA
                           3106
                          , --CHAPADMALAL
                           2397
                          , --MIRAMAR (BOLIVAR-PDO. BOLIVAR)
                           3116
                          , --MIRAMAR (PDO. GRAL. ALVARADO)
                           3125
                          , --SANTA CLARA DEL MAR
                           3180
                          , --BAHIA BLANCA
                           3130
                          , --CAMET
                           3086
                          , --BATAN
                           2777
                          , --GENERAL PIRAN
                           3093
                          , --SIERRA DE LOS PADRES
                           12333
                          , -- MONTE FLORES
                           2654
                          , --SANTA TERESITA
                           11907
                          , --CIPOLLETTI
                           11896
                          , --CATRIEL
                           16193
                          , --RIO CEBALLOS
                           17516
                          , --LA CALERA (GIGENA-DPTO. RIO CUARTO)
                           1806
                          , --DOMSELAAR
                           1537
                          , --AEROPUERTO EZEIZA
                           16585
                          , --ALTA GRACIA
                           970
                          , --CA�UELAS
                           18998
                          , --MAR DE COBO
                           1448
                          , --MERLO
                           16192
                          , --PAJAS BLANCAS
                           3087
                          , --PUNTA MOGOTES
                           16196
                          , --SALSIPUEDES
                           12796
                          , --GODOY
                           4428
                          , --MERLO
                           1719
                          , --JOAQUIN GORINA
                           1976
                          , --CARLOS LEMEE
                           2664
                          , --AGUAS VERDES 
                           2653
                          , --LAS TONINAS
                           2657
                          , --MAR DE AJO
                           2773
                          , --PINAMAR
                           2662
                          , --SAN BERNARDO (PDO. DE LA COSTA)
                           2769 --VILLA GESELL -->NEW
                           )
       
       OR l.nume_prov IN (14) --NEUQUEN
       --12) --MENDOZA
       
       )
 GROUP BY s.fech_serv
         ,SYSDATE
         ,bat.nume_serv
         ,
          --          SS.NUME_SERV,
          s.nume_diag_esti
         ,to_char(s.fech_serv, 'DAY')
         ,
          --          TO_CHAR(SH.HORA_PEDI,'HH24'),
          s.fech_serv
         ,l.nume_regi
         ,co.nume_comp
         ,zon.nomb_zona
         ,zon.nume_macr_zona
         ,s.nume_serv
         ,sd.nume_lati
         ,sd.nume_long
         ,r.nomb_regi
         ,l.nomb_loca
         ,l.nume_loca
         ,co.nomb_comp
         ,
          --          CR.NUME_RADI,
          cr.nomb_radi
         ,
          --  sev.obse_even,
          tc.nomb_tipo_cont
         ,ts.nomb_tipo_serv
         ,ss.nomb_titu
         ,ss.tele_titu
         ,ss.pate_vehi_actu
         ,ss.marc_vehi
         ,ss.mode_vehi
         ,ss.tipo_tele_titu
         ,ss.mail_soci
         ,ss.codi_iden_auto
         ,sg.nomb_diag
         ,pers.nomb_pers
         ,p.nomb_pres
         ,l.nume_prov
         ,prov.nomb_prov
         ,s.nume_diag_real
         ,s.nume_tele
         ,sh.hora_pedi
         ,loca.localidades_agrupadas
-- SP.NUME_PROD

--ORDER BY  TO_CHAR(SH.HORA_PEDI,'HH24') ASC


